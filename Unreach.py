from lib import (Parkour, Tracker)

USED_FUNC = None
ALL_FUNC = None

#./automatic-debugger Unreach AutomaticDebugger/tests/unreachability/main.c -v debug

def fortrack(self, node, parent, *args, **kwargs):
    """
    Forward tracking function that manage scope. It defines if the scope is
    named or not and push it into a stack.

    * BlockStmt node defines scope embraces in braces
    """
    if type(node).__name__ is 'BlockStmt' and parent is not None:
        scope_manager = self.scope(parent)
        scope_manager(parent)

def backtrack(self, node, parent, *args, **kwargs):
    """
    Back tracking function that pop scope when it's closed
    """
    if type(node).__name__ is 'BlockStmt' and parent is not None:
        self._scopes.pop()

class Unreach(Parkour):

    def __init__(self, filename, verbosity, report_file, *args):
        super(Unreach, self).__init__(filename, verbosity)
        global USED_FUNC;
        USED_FUNC = 1;
        global ALL_FUNC;
        ALL_FUNC = 0;
        self.graph = [[],[]]
        self._scope_switcher = {
            'Decl': self._named_scope,
        }

    def scope(self, node):
        """
        retrieve the function which define if the whole scope is named or not
        """
        def trap(node, *args, **kwargs):
            self._logger.debug("scope : {node}"\
                               .format(node=node))
        return self._scope_switcher.get(type(node).__name__,
                                        trap)

    def _named_scope(self, node, **kwargs):
        """
        push the name of the named scope
        """
        self._scopes.append(node._name)
        self._logger.debug("named scope : {}".format(node._name))

    # NEVER GO IN
    # NEVER GO IN

    # def _while(self, node, parent, *args, **kwargs):
    #     super(Unreach, self)._decl(node, parent, *args, **kwargs)
    #     name_func = str(node._name);
    #     print ('-----')
    #     print (name_func);
    #     print ('PUSHED IN TAB from WHILE')
    #     print (" GOOOOO ")
    #     print (" GOOOOO ")
    #     print (" GOOOOO ")
    #     print (" GOOOOO ")
    #     print (node);
    #     self.graph[USED_FUNC].append(name_func);

    #
    #

    def _decl(self, node, parent, *args, **kwargs):
        super(Unreach, self)._decl(node, parent, *args, **kwargs)
        if hasattr(node, 'body'):
          name_func = str(node._name);
          if (node._name != 'main'):
            self.graph[ALL_FUNC].append(name_func);
        if hasattr(node, '_assign_expr'):
          #print (node._assign_expr)
          if hasattr(node._assign_expr, 'call_expr'):
            #print ('IIIIIIICII');
            #print ('IIIIIIICII');
            #print ('IIIIIIICII');
            #print (node._assign_expr.call_expr)
            #print ('-----')
            call_exp = str(node._assign_expr.call_expr);
            res = (call_exp[call_exp.find("'")+1:call_exp.rfind("'")]);
            #print ('PUSHED IN TAB from DECL')
            self.graph[USED_FUNC].append(res);

    # def _unary(self, node, parent, *args, **kwargs):
    #     super(Unreach, self)._decl(node, parent, *args, **kwargs)
    #     print('IIIIICICICICICI');
    #     print('IIIIICICICICICI');
    #     print('IIIIICICICICICI');
    #     print (node);
    #     if hasattr(node, 'params'):
    #       call_exp = str(node.params);
    #       res = (call_exp[call_exp.find("'")+1:call_exp.rfind("'")]);
    #       #print ('PUSHED IN TAB from DECL')
    #       self.graph[USED_FUNC].append(res);

    def _binary(self, node, parent, *args, **kwargs):
        super(Unreach, self)._decl(node, parent, *args, **kwargs)
        for z in range(len(node.params)):
          param = node.params[z]
          if (type(param).__name__ is 'Func'):
            call_exp = str(param.call_expr);
            res = (call_exp[call_exp.find("'")+1:call_exp.rfind("'")]);
            #print ('-----')
            #print (res);
            #print ('PUSHED IN TAB from BINARY')
            self.graph[USED_FUNC].append(res);


    def _func(self, node, parent, *args, **kwargs):
        super(Unreach, self)._decl(node, parent, *args, **kwargs)
        call_exp = str(node.call_expr);
        res = (call_exp[call_exp.find("'")+1:call_exp.rfind("'")]);
        #print ('-----')
        #print (res);
        #print ('PUSHED IN TAB from FUNC')
        self.graph[USED_FUNC].append(res);

    def run(self, *args, **kwargs):
        self.endings = {}
        super(Unreach, self).run(*args, **kwargs)
        self.render()

    @Tracker(fortrack, backtrack)
    def switch_func(self, node, parent, *args, **kwargs):
        return super(Unreach, self).switch_func(node, parent, *args, **kwargs)

    def render(self):
      funcs_not_used = list(set(self.graph[ALL_FUNC]) - set(self.graph[USED_FUNC]))

      #print('List of all functions :');
      #print(self.graph[ALL_FUNC]);
      #print ('');
      #print('List of called functions :');
      #print(self.graph[USED_FUNC]);


      if (len(funcs_not_used) > 0):
        print ('')
        print ('')
        print ('Results !')
        print ('RECAP : ')
        print('List of all functions :');
        print(set(self.graph[ALL_FUNC]));
        print ('');
        print('List of called functions :');
        print(set(self.graph[USED_FUNC]));
        print ('');
        print ('');

        print ('SOME FUNCTIONS ARE NEVER USED :');
        print (funcs_not_used);
        print ('')
      else:
        print ('')
        print ('')
        print ('All functions are used');
        print ('');
        print ('');

        # # EXEMPLE :
        # Decl(body =
        #     BlockStmt(
        #         body = [
        #             Decl(
        #                 _ctype = PrimaryType(
        #                     _storage = 0,
        #                     _specifier = 0,
        #                     _decltype = None,
        #                     _identifier = 'int'
        #                 ),
        #                 _name = 'a'
        #             ),
        #             ExprStmt(
        #                 expr = Binary(
        #                     params = [
        #                         Id(value = 'a'),
        #                         Literal(value = '0')
        #                     ],
        #                     call_expr = Raw(value = '=')
        #                 )
        #             ),
        #             ExprStmt(
        #                 expr = Binary(
        #                     params = [
        #                         Id(value = 'a'),
        #                         Binary(params = [
        #                             Func(
        #                                 params = [
        #                                     Id(value = 'a')
        #                                 ],
        #                                 call_expr = Id(value = 'used_code')
        #                             ),
        #                             Literal(value = '1')
        #                             ],
        #                             call_expr = Raw(value = '+')
        #                         )
        #                     ],
        #                     call_expr = Raw(value = '=')
        #                 )
        #             )
        #         ],
        #         types = ChainMap({}, {})
        #     ),
        #     _ctype = FuncType(
        #         _params = [
        #             Decl(_ctype = PrimaryType(
        #                 _storage = 0,
        #                 _specifier = 0,
        #                 _decltype = None,
        #                 _identifier = 'void'
        #                 ),
        #             _name = ''
        #             )
        #         ],
        #         _storage = 0,
        #         _specifier = 0,
        #         _decltype = None,
        #         _identifier = 'int'
        #     ),
        #     _name = 'main'
        # )

